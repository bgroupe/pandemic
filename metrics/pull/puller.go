package main

import (
	"bufio"
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/iancoleman/strcase"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/prometheus/common/log"
	"gitlab.com/bgroupe/pandemic/metrics/config"
)

var (
	spf          = fmt.Sprintf
	listenerPort string
	configFile   string
	filePath     string
	cfg          config.PushConfig
)

func init() {
	flag.StringVar(&listenerPort, "port", "9191", "specifies the port of the for the metric server to listen on")
	flag.StringVar(&configFile, "config", "config.yaml", "[REQUIRED] path to a config file")
	flag.Parse()
}

func main() {
	config.ReadConfig(&cfg, configFile)
	go recordMetrics()
	// This section will start the HTTP server and expose
	// any metrics on the /metrics endpoint.
	port := spf(":%s", listenerPort)
	http.Handle("/metrics", promhttp.Handler())
	log.Info("Beginning to serve on port", port)
	log.Fatal(http.ListenAndServe(port, nil))
}

func recordMetrics() {
	for {
		now := time.Now()
		if cfg.FilePath != "" {
			filePath = cfg.FilePath
		} else {
			filePath = spf("%02d-%02d-%d.csv", now.Month(), now.Day(), now.Year())
		}

		fileUrl := spf("%s/%s", cfg.BaseUrl, filePath)

		if err := downloadFile(filePath, fileUrl); err != nil {
			panic(err)
		}

		csvFile, _ := os.Open(filePath)
		reader := csv.NewReader(bufio.NewReader(csvFile))
		lines, _ := reader.ReadAll()
		var headers []string

		for _, header := range lines[0] {
			headers = append(headers, strings.ToLower(header))
		}

		for _, line := range lines[1:] {
			for _, rh := range cfg.MetricsHeaders {
				index := Index(headers, rh)
				if index == -1 {
					panic("metric not found")
				}
				state := strcase.ToSnake(line[0])
				statistic, _ := strconv.ParseFloat(line[index], 64)
				metricName := fmt.Sprintf("%s_%s", state, rh)
				newMetric := prometheus.NewGauge(prometheus.GaugeOpts{
					Name: metricName,
					Help: metricName,
				})
				err := prometheus.Register(newMetric)
				if err != nil {
					_, ok := err.(prometheus.AlreadyRegisteredError)
					if ok {
						// I am swallowing duplicate registration errors here
					} else {
						panic(err)
					}
				}
				newMetric.Set(statistic)
				if cfg.DebugLogging {
					log.Info("metric logged for statistic: ", metricName)
				}
			}
		}
		log.Info("Successfully generated metrics")

		time.Sleep(time.Duration(cfg.CollectionInterval) * time.Second)
	}
}

// Save file to root for processing
func downloadFile(filepath string, url string) error {

	r, err := http.Get(url)

	if err != nil {
		return err
	}
	if r.StatusCode != 200 {
		log.Warn("file not found:", filepath)
	}
	defer r.Body.Close()

	output, err := os.Create(filepath)

	if err != nil {
		return err
	}

	defer output.Close()

	_, err = io.Copy(output, r.Body)

	return err
}

// Basic Collection Functions
func Index(vs []string, t string) int {
	for i, v := range vs {
		if v == t {
			return i
		}
	}
	return -1
}
