package config

import (
	"fmt"
	"os"

	"gopkg.in/yaml.v2"
)

type PushConfig struct {
	BaseUrl            string   `yaml:"base_url"`
	CollectionInterval int64    `yaml:"collection_interval"`
	MetricsHeaders     []string `yaml:"metrics_headers"`
	FilePath           string   `yaml:"file_path"`
	DateFormat         string   `yaml:"date_format"`
	PushGatewayUrl     string   `yaml:"push_gateway_url"`
	MetricsJobName     string   `yaml:"metrics_job_name"`
	DebugLogging       bool     `yaml:"debug_logging"`
}

func ReadConfig(cfg *PushConfig, filePath string) {
	f, err := os.Open(filePath)

	if err != nil {
		processError(err)
	}

	defer f.Close()
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(cfg)

	if err != nil {
		processError(err)
	}
}

// Private
func processError(err error) {
	fmt.Println(err)
	os.Exit(2)
}
