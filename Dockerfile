FROM golang:1.13-alpine

RUN mkdir /app
ADD . /app/
WORKDIR /app

RUN cd metrics/pull && go build -v
RUN cd metrics/push && go build -v

RUN mv metrics/push/push /app/push && mv metrics/pull/pull /app/pull

# Start metrics server
# CMD ["/app/pull", "--port", "<port>", "--config", "<config-file.yaml>"]

# One-time push of metrics
# CMD ["/app/push", "--port", "<port>", "--config", "<config-file.yaml>"]
